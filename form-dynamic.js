{
  const {
    html,
  } = Polymer;
  /**
    `<form-dynamic>` Description.

    Example:

    ```html
    <form-dynamic></form-dynamic>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --form-dynamic | :host    | {} |

    * @customElement form-dynamic
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class FormDynamic extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'form-dynamic';
    }

    static get properties() {
      return {
        fieldsArray: {
          type: Array,
          value: []
        },
        buttonText: {
          type: String,
          value: 'Enviar'
        },
        formToSend: {
          type: Object,
          value: {}
        }
      };
    }

    getValues() {
      this.shadowRoot.querySelectorAll('cells-molecule-input')
        .forEach(field => {
          console.log(field.value);
          this.formToSend[field.label] = field.value;
        });
      console.log(this.formToSend);
      this._dispatchEvent('send-data', this.formToSend);
    }

    _dispatchEvent(name, data, bubbles = true, composed = true) {
      this.dispatchEvent(new CustomEvent(name, {
        detail: data,
        bubbles: bubbles,
        composed: composed
      }));
    }
  }

  customElements.define(FormDynamic.is, FormDynamic);
}